﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CalculatorApp
{
    public partial class MainPage : ContentPage
    {
        string userInput = string.Empty;
        string op1 = string.Empty;
        string op2 = string.Empty;
        string operation; 
        double num1, num2, total;

        public MainPage()
        {
            
            InitializeComponent();

        }

        private void Button_Seven_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput+= "7" ;
            this.display_Num.Text += userInput; 

        }

      
        private void Button_Eight_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "8";
            this.display_Num.Text += userInput;
        }

        private void Button_Nine_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "9";
            this.display_Num.Text += userInput;
        }
        private void Button_Six_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "6";
            this.display_Num.Text += userInput;
        }

        private void Button_Four_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "4";
            this.display_Num.Text += userInput;
        }

        private void Button_Five_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "5";
            this.display_Num.Text += userInput;
        }

        private void Button_One_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            userInput += "1";
            this.display_Num.Text += userInput;
        }

        private void Button_Two_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "2";
            this.display_Num.Text += userInput;
        }

        private void Button_Three_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "3";
            this.display_Num.Text += userInput;
        }

        private void Button_Zero_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += "0";
            this.display_Num.Text += userInput;
        }

        private void Button_Period_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "";
            this.userInput += ".";
            this.display_Num.Text += userInput; 
        }

        private void Button_Equal_Clicked(object sender, EventArgs e)
        {
            op2 = userInput;
            double.TryParse(op1, out num1);
            double.TryParse(op2, out num2);

            if (operation == "add")
            {
                total = num1 + num2;
                display_Num.Text = total.ToString();

            }
            else if(operation == "sub")
            {
                total = num1 - num2;
                display_Num.Text = total.ToString(); 
            }
            else if (operation == "mult")
            {
                total = num1 * num2;
                display_Num.Text = total.ToString(); 
            }
            else if (operation == "div")
            {
                if(num2 != 0)
                {
                    total = num1 / num2;
                    display_Num.Text = total.ToString();
                }
                else
                {
                    display_Num.Text = "ERROR";
                }
            }
            else if( operation == "%")
            {
                total = total / 100;
                display_Num.Text = total.ToString(); 

            }


        }

        private void Button_Add_Clicked(object sender, EventArgs e)
        {
            op1 = userInput;
            operation = "add";
            userInput = string.Empty; 
            
        }

        private void Button_Sub_Clicked(object sender, EventArgs e)
        {
            op1 = userInput;
            operation = "sub";
            userInput = string.Empty;
        }

        private void Button_Mult_Clicked(object sender, EventArgs e)
        {
            op1 = userInput;
            operation = "mult";
            userInput = string.Empty;
        }

        private void Button_Div_Clicked(object sender, EventArgs e)
        {
            op1 = userInput;
            operation = "div";
            userInput = string.Empty;
        }

        private void Button_Percent_Clicked(object sender, EventArgs e)
        {
            op1 = userInput;
            operation = "%";
            userInput = string.Empty; 


        }

        private void Button_Neg_Pos_Clicked(object sender, EventArgs e)
        {

        }

        private void Button_Clear_Clicked(object sender, EventArgs e)
        {
            this.display_Num.Text = "0";
            this.userInput = string.Empty; 

        }
    }

}
